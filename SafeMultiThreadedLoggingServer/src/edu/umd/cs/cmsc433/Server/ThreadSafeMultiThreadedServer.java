package edu.umd.cs.cmsc433.Server;

// Handles writing records to log files in separate threads
// Class in NOT thread safe

import edu.umd.cs.cmsc433.LoggingServerUtils.DataRecord;
import edu.umd.cs.cmsc433.LoggingServerUtils.LockingMsgHandler;

public class ThreadSafeMultiThreadedServer extends LoggingServerCore {
	
	ThreadSafeMultiThreadedServer(int port) {
		super(port);
	}



public void process(DataRecord record) {
	new Thread(new LockingMsgHandler(record)).start();
}

	public static void main(String[] args) {
		System.out.println("Starting server on port:" + PORT);
		ThreadSafeMultiThreadedServer mts = new ThreadSafeMultiThreadedServer(
				PORT);
		mts.go();
		System.out.println("Shutting down server on port:" + PORT);
	}
}

