package cmsc433.rmichat.common;
import java.rmi.Remote;
import java.rmi.RemoteException;

/** 
  * A connection is the interface used by a client
  * to talk to the chat server once a connection
  * has been established.
  */ 
public interface Connection extends Remote {
	/** 
	  * Invoked by client to say something to everyone in chat room
	  * @param msg Message to be said
	  */
        public void say(String msg) throws RemoteException;
	/** 
	  * Invoked by client to say something to one person in chat room
	  * @param who Name of person to say it to
	  * @param msg Message to be said
	  */
        public void say(String who, String msg) throws RemoteException;
	/**
	  * Returns list of everyone in room
	  */
        public String [] users() throws RemoteException;
	/**
	  * Invoked by client to terminate connection
	  */
	public void logoff() throws RemoteException;
}
