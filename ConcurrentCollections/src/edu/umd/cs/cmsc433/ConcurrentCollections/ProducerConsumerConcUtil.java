package edu.umd.cs.cmsc433.ConcurrentCollections;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class ProducerConsumerConcUtil {
	static int num;
	final static CountDownLatch start = new CountDownLatch(1);

	static synchronized int getNext() {
		return ++num;
	}

	public static void main(String[] args) throws InterruptedException {

		Buffer c = new Buffer(10);
		Producer p1 = new Producer(c, 1);
		Consumer c1 = new Consumer(c, 1);
		Consumer c2 = new Consumer(c, 2);
		Consumer c3 = new Consumer(c, 3);
		Thread t1 = new Thread(p1);
		t1.start();
		Thread t2 = new Thread(c1);
		t2.start();
		Thread t3 = new Thread(c2);
		t3.start();
		Thread t4 = new Thread(c3);
		t4.start();

		start.countDown();
		Thread.sleep(500);

		t1.interrupt();
		t2.interrupt();
		t3.interrupt();
		t4.interrupt();

	}

	static class Buffer {
		private BlockingQueue<Integer> contents;

		public Buffer(int capacity) {
			this.contents = new LinkedBlockingQueue<Integer>(capacity);
		}

		public int get() throws InterruptedException {
			return contents.take();
		}

		public void put(int value) throws InterruptedException {
			contents.put(value);
		}
	}

	static class Consumer implements Runnable {
		private Buffer buffer;
		private int number;

		public Consumer(Buffer c, int number) {
			buffer = c;
			this.number = number;
		}

		public void run() {
			try {
				start.await();
			} catch (InterruptedException e1) {
			}
			while (!Thread.interrupted()) {
				try {
					System.out.println("Consumer #" + this.number + " got: "
							+ buffer.get());
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
			System.out.println(this + ":exited Consumer");
		}
	}

	static class Producer implements Runnable {
		private Buffer buffer;
		private int number;

		public Producer(Buffer c, int number) {
			buffer = c;
			this.number = number;
		}

		public void run() {
			int i;
			try {
				start.await();
			} catch (InterruptedException e1) {
			}
			while (!Thread.interrupted()) {
				i = ProducerConsumerConcUtil.getNext();
				try {
					buffer.put(i);
					System.out.println("Producer #" + this.number + " put: "
							+ i);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
			System.out.println(this + ":exited Producer");
		}
	}
}
