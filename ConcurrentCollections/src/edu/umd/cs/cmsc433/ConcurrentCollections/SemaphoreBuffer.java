package edu.umd.cs.cmsc433.ConcurrentCollections;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class SemaphoreBuffer {

	private int size = 5;
	final static CountDownLatch start = new CountDownLatch(1);
	private ConcurrentLinkedQueue<Integer> contents = new ConcurrentLinkedQueue<Integer>();
	
	Semaphore emptySlots = new Semaphore(size);
	Semaphore fullSlots = new Semaphore(0);

	public void put(Integer value) throws InterruptedException {
		emptySlots.acquire();
		contents.add(value);
		fullSlots.release();
	}

	public Integer take() throws InterruptedException {
		Integer value;
		fullSlots.acquire();
		value = contents.remove();
		emptySlots.release();
		return value;
	}

	static class Consumer implements Runnable {
		private SemaphoreBuffer buffer;
		
		public Consumer(SemaphoreBuffer c) {
			buffer = c;
		}

		public void run() {
			try {
				start.await();
				while (!Thread.interrupted()) {
					System.out.println("Consumer got: "
							+ buffer.take());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		static class Producer implements Runnable {
			private SemaphoreBuffer buffer;

			public Producer(SemaphoreBuffer c) {
				buffer = c;
				}

			public void run() {
				int i;
				try {
					start.await();
					while (!Thread.interrupted()) {
						i = ProducerConsumerPrimitive.getNext();
						buffer.put(i);
						System.out.println("Producer put: " + i);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		public static void main(String[] args) throws InterruptedException {
			SemaphoreBuffer c = new SemaphoreBuffer();
			Producer p1 = new Producer(c);
			Consumer c1 = new Consumer(c);
			Thread tp1 = new Thread(p1);
			tp1.start();
			Thread tc1 = new Thread(c1);
			tc1.start();
			start.countDown();
			Thread.sleep(50);
			tp1.interrupt();
			tc1.interrupt();
			System.out.println("Main method ending");

		}
	}
}
