package net.jcip.examples;

import java.awt.Point;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class SimpleMaze implements IPuzzle<Point, Move2DEnum> {
	private final static int DIM = 20;

	Point mGoal = new Point(new Random().nextInt(DIM),
			new Random().nextInt(DIM));
	Point mInitialPos = new Point(new Random().nextInt(DIM),
			new Random().nextInt(DIM));

	public SimpleMaze() {
		System.out.println("Initial:" + mInitialPos);
		System.out.println("Goal:" + mGoal);
	}

	@Override
	public Point initialPosition() {
		return mInitialPos;
	}

	@Override
	public boolean isGoal(Point position) {
		return position.equals(mGoal);
	}

	@Override
	public Set<Move2DEnum> legalMoves(Point position) {
		Set<Move2DEnum> tmp = new HashSet<Move2DEnum>();
		if (position.x > 0)
			tmp.add(Move2DEnum.LEFT);
		if (position.x < DIM)
			tmp.add(Move2DEnum.RIGHT);
		if (position.y > 0)
			tmp.add(Move2DEnum.DOWN);
		if (position.y < DIM)
			tmp.add(Move2DEnum.UP);
		return tmp;
	}

	@Override
	public Point move(Point position, Move2DEnum move) {

		switch (move) {
		case LEFT:
			return new Point(position.x - 1, position.y);
		case RIGHT:
			return new Point(position.x + 1, position.y);
		case UP:
			return new Point(position.x, position.y + 1);
		case DOWN:
			return new Point(position.x, position.y - 1);
		default:
			return null;
		}
	}
}
