package net.jcip.examples;

import java.util.concurrent.atomic.*;

public class ConcurrentPuzzleSolverForPuzzlesWithNoSolution <POS_REP,MOVE_REP> extends ConcurrentPuzzleSolver<POS_REP, MOVE_REP> {

	ConcurrentPuzzleSolverForPuzzlesWithNoSolution(IPuzzle<POS_REP, MOVE_REP> puzzle) {
        super(puzzle);
    }

    private final AtomicInteger taskCount = new AtomicInteger(0);

    protected Runnable newTask(POS_REP p, MOVE_REP m, PuzzleNode<POS_REP, MOVE_REP> n) {
        return new CountingSolverTask(p, m, n);
    }

    class CountingSolverTask extends SolverTask {
        CountingSolverTask(POS_REP pos, MOVE_REP move, PuzzleNode<POS_REP, MOVE_REP> prev) {
            super(pos, move, prev);
            taskCount.incrementAndGet();
        }

        public void run() {
            try {
                super.run();
            } finally {
                if (taskCount.decrementAndGet() == 0)
                    solution.setValue(null);
            }
        }
    }
}
