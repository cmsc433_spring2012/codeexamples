package edu.umd.cs.cmsc433.Alarms;
import java.io.*;

 class ThreadTestRunnable {
	private static BufferedReader b = new BufferedReader(new InputStreamReader(
			System.in));
	private String msg = null;
	private int timeout = 0;

	class AlarmRunnable implements Runnable {
		private String msg = null;
		private int timeout = 0;

		public AlarmRunnable(String msg, int timeout) {
			this.msg = msg;
			this.timeout = timeout;
		}

		public void run() {
			try {
				Thread.sleep(timeout * 1000);
			} catch (InterruptedException e) {
			}
			System.out.println("(" + timeout + ") " + msg);
		}
	}

	private void parseInput(String line) {
		try {
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) == ' ') {
					timeout = Integer.parseInt(line.substring(0, i));
					msg = line.substring(i + 1);
					break;
				}
			}
		} catch (NumberFormatException e) {
			System.out.println("bad input " + line);
		}
	}

	private void doAlarm() throws IOException {
		System.out
				.println("Enter alarms of the form `time msg', e.g., `3 hello'");
		while (true) {
			System.out.print("Alarm> ");
			String line = b.readLine();
			if (line == null)
				return;
			parseInput(line);
			Thread t = null;
			if (msg != null) {
				t = new Thread(new AlarmRunnable(msg, timeout));
			}
			if (t != null)
				t.start();
		}
	}

	public static void main(String args[]) throws IOException {
		ThreadTestRunnable ttr = new ThreadTestRunnable();
		ttr.doAlarm();
	}
}
