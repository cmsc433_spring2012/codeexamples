package edu.umd.cs.cmsc433.LoggingServerUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class MsgHandler implements Runnable {

	protected final DataRecord record;
	protected PrintWriter out;

	public MsgHandler(DataRecord record) {
		this.record = record;
	}

	public void run() {

		System.out.println("Server Side: writing record:" + record);
		writeMsg();
	}

	protected void writeMsg() {
		try {
			out = new PrintWriter(new FileWriter(record.getSender()
					+ ".txt", true));
			out.print("Begin Record:");
			out.flush();
			out.print("Sender:" + record.getSender() + ":");
			out.flush();
			out.print("Message:" + record.getMsg() + ":");
			out.flush();
			out.println("End Record");
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			out.close();
		}
	}
}