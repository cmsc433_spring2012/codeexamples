import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PrimeBuggy implements Runnable {
	private static int start, N;
	private static int numThreads;
	private static List<Integer> primes;
	private static List<Boolean> notPrime;
	private static int total = 0;

	private static boolean is_prime(int v) {
		int i;
		int bound = (int) Math.floor(Math.sqrt((double) v)) + 1;
		for (i = 2; i < bound; i++) {
			if (notPrime.get(i))
				continue;
			if (v % i == 0) {
				notPrime.set(v, true);
				return false;
			}
		}
		return (v > 1);
	}

	private int threadNum;

	private PrimeBuggy(int threadNum) {
		this.threadNum = threadNum;
	}

	public void run() {
		int i;
		start = N / numThreads * threadNum;
		N = start + N / numThreads;
		for (i = start; i < N; i++) {
			if (is_prime(i)) {
				primes.add(i);
				total++;
			}
		}
	}

	public static void main(String args[]) throws InterruptedException {
		if (args.length != 2) {
			System.out.println("Args: numPrimes numThreads");
			System.exit(1);
		}

		N = new Integer(args[0]).intValue();
		primes = new ArrayList<Integer>();
		notPrime = new ArrayList<Boolean>(N);
		for (int i = 0; i < N; i++) {
			notPrime.add(false);
		}

		numThreads = new Integer(args[1]).intValue();
		Thread[] threads = new Thread[numThreads - 1];

		for (int i = 0; i < numThreads; i++) {
			if (i != numThreads - 1) {
				threads[i] = new Thread(new PrimeBuggy(i));
				threads[i].start();
			} else {
				new PrimeBuggy(i).run();
			}
		}

		System.out.println("Number of prime numbers between 2 and " + N + ": "
				+ total);
		Collections.sort(primes);
		System.out.println(primes);
	}
}