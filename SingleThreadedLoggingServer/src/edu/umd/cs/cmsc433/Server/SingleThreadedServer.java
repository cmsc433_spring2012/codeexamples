package edu.umd.cs.cmsc433.Server;

import edu.umd.cs.cmsc433.LoggingServerUtils.DataRecord;
import edu.umd.cs.cmsc433.LoggingServerUtils.MsgHandler;

public class SingleThreadedServer extends LoggingServerCore {

	SingleThreadedServer(int port) {
		super(port);
	}


	public void process(DataRecord record) {
		(new MsgHandler (record)).run();
	}
	
	
	public static void main(String[] args) {
		System.out.println("Starting server on port:" + PORT);
		
		SingleThreadedServer s = new SingleThreadedServer(PORT);
		s.go();
		
		System.out.println("Shutting down server on port:" + PORT);
	}
}