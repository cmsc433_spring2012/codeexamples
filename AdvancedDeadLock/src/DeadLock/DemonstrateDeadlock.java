package DeadLock;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DemonstrateDeadlock {
	private static final int NUM_THREADS = 20;
	private static final int NUM_ACCOUNTS = 5;
	private static final int NUM_ITERATIONS = 10000;

	public static void main(String[] args) {
		final Random rnd = new Random();
		final Account[] accounts = new Account[NUM_ACCOUNTS];

		for (int i = 0; i < accounts.length; i++)
			accounts[i] = new Account();

		ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
		for (int i = 0; i < NUM_THREADS; i++) {
			executor.execute(new Runnable() {
				@Override
				public void run() {
					for (int i = 0; i < NUM_ITERATIONS; i++) {
						int fromAcct = rnd.nextInt(NUM_ACCOUNTS);
						int toAcct = rnd.nextInt(NUM_ACCOUNTS);
						DollarAmount amount = new DollarAmount(rnd
								.nextInt(1000));
						try {
							
/*							InduceLockOrder.transferMoney(accounts[fromAcct],
									accounts[toAcct], amount);
*/							
							
							DynamicOrderDeadlock.transferMoney(
									accounts[fromAcct], accounts[toAcct],
									amount);
						
						} catch (InsufficientFundsException ignored) {
						}
					}
					System.out.println("transactions finished");
				}
			});
		}
		executor.shutdown();
	}
}
