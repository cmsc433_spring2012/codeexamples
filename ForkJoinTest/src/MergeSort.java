import java.util.Arrays;
import java.util.Random;

import jsr166y.ForkJoinPool;
import jsr166y.RecursiveAction;

public class MergeSort extends RecursiveAction {

	private static final long serialVersionUID = 1L;
	final int[] numbers;
	final int startPos, endPos;
	final int[] result;

	final int SEQUENTIAL_THRESHOLD = 10;

	public MergeSort(int[] numbers, int startPos, int endPos) {
		super();
		this.numbers = numbers;
		this.startPos = startPos;
		this.endPos = endPos;
		result = new int[numbers.length];
	}

	private void merge(MergeSort left, MergeSort right) {
		int i = 0, leftPos = 0, rightPos = 0, leftSize = left.size(), rightSize = right
				.size();
		while (leftPos < leftSize && rightPos < rightSize)
			result[i++] = (left.result[leftPos] <= right.result[rightPos]) ? left.result[leftPos++]
					: right.result[rightPos++];
		while (leftPos < leftSize)
			result[i++] = left.result[leftPos++];
		while (rightPos < rightSize)
			result[i++] = right.result[rightPos++];
	}

	public int size() {
		return endPos - startPos;
	}

	protected void compute() {
		if (size() < SEQUENTIAL_THRESHOLD) {
			System.arraycopy(numbers, startPos, result, 0, size());
			Arrays.sort(result, 0, size());
		} else {
			int midpoint = size() / 2;
			MergeSort left = new MergeSort(numbers, startPos, startPos
					+ midpoint);
			MergeSort right = new MergeSort(numbers, startPos + midpoint,
					endPos);
			invokeAll(left, right);
			merge(left, right);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		int len = 10000;
		int[] nums = new int[len];
		Random r = new Random();

		for (int i = 0; i < len; i++) {
			nums[i] = r.nextInt(len);
		}

		ForkJoinPool pool = new ForkJoinPool();
		MergeSort whole = new MergeSort(nums, 0, nums.length);
		long startTime = System.currentTimeMillis(); 
		pool.invoke(whole);
		long endTime = System.currentTimeMillis();
		System.out.println("Time:" + (endTime-startTime));
		System.out.println(Arrays.toString(whole.result));
	}
}